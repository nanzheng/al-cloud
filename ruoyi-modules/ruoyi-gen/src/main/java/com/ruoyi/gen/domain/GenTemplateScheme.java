package com.ruoyi.gen.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.security.entity.BaseZrdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代码生成模板组管理对象 gen_template_scheme
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenTemplateScheme extends BaseZrdEntity implements Serializable {

    private static final long serialVersionUID = -4163234872260688466L;
    /**
     * 主键
     */
    private Long id;

    /**
     * 方案标题
     */
    @Excel(name = "方案标题" )
    private String title;

    /**
     * 方案描述
     */
    @Excel(name = "方案描述" )
    private String description;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;


}
