package com.ruoyi.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.gen.domain.GenTemplate;
import com.ruoyi.gen.domain.entity.GenTemplateEntity;

import java.util.List;

/**
 * 代码生成模板管理1Service接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface IGenTemplateService extends IService<GenTemplate> {


    /**
     * 查询代码生成模板管理1列表
     *
     * @param genTemplate 代码生成模板管理1
     * @return 代码生成模板管理1集合
     */
    List<GenTemplateEntity> selectGenTemplateList(GenTemplateEntity genTemplate);

    /**
     * 描述： 查询模板下的 模板列表
     * 备注：
     * 日期： 14:29 2020/12/28
     * 作者： zrd
     *
     * @param schemeId
     * @return java.util.List<com.ruoyi.gen.domain.GenTemplate>
     **/
    List<GenTemplate> selectGenTemplateListBySid(Long schemeId);


}
