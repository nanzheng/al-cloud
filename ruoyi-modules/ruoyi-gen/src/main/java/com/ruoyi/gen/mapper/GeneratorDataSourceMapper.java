package com.ruoyi.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.gen.domain.GeneratorDataSource;

import java.util.List;

/**
 * 数据源Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface GeneratorDataSourceMapper extends BaseMapper<GeneratorDataSource> {

    /**
     * 查询数据源列表
     *
     * @param generatorDataSource 数据源
     * @return 数据源集合
     */
    List<GeneratorDataSource> selectGeneratorDataSourceList(GeneratorDataSource generatorDataSource);


}
