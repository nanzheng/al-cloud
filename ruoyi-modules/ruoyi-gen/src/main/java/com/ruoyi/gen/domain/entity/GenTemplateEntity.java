package com.ruoyi.gen.domain.entity;

import com.ruoyi.gen.domain.GenTemplate;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代码生成模板管理对象 gen_template
 *
 * @author ruoyi
 * @date 2020-06-22
 */
@ApiModel("代码生成模板管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenTemplateEntity extends GenTemplate implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 模板类型的翻译字段 字典的code为gen_type
     **/
    private String templateTypeName;


}
