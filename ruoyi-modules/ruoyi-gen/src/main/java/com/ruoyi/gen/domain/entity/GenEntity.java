package com.ruoyi.gen.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代码生成模板管理1对象 gen_template
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenEntity implements Serializable {

    private static final long serialVersionUID = -9166500578771654364L;

    private String[] tableNames;
    private String tables;
    private Long sourceId;

}
